% Base types
% -----------------------------------------------------------------------------

\nge{cmpval}{name=composite value, description={
    Value of a composite data type
  }}
\nge{prmval}{name=primitive value,description={
    Value of a primitive data type
  }}
\nge{atmval}{name=atomic value,description={
    Value of an atomic data type
  }}
\nge{dict}{name=dictionary,plural=dictionaries,description={
    Mapping, association, of keys to values
  }}
\nge{imm}{name=immutable,description={
    Immutable value, that can never change after it is constructed
  }}

% Tree types
% -----------------------------------------------------------------------------

\nge{arb}{name=arborescence,description={
    A tree that has a root and where edges point away from the root.
    Also known as a ``directed forward rooted tree'' or ``out-tree''.
    Its nodes are colored, labeled and its leafs can store a
    \gls{prmval}. This tree is used to represent an arbitrary structure
    of primitive (leafs) and composite (non-leafs) values
  }}
\nge{parb}{name=poly-arborescence,parent=arb,description={
    Disjoint union of a collection of \glspl{arb}. Can be defined as
    a directed root tree whose edges point away from the root,
    or as an \gls{arb} with multiple roots, whose subtrees
    don't share any node
  }}
\nge{pth}{name=path,parent=arb,description={
    In an \gls{arb}, a node's path is the sequence of node labels from the root
    to this node
  }}
\nge{src}{name=source,parent=arb, description={
    The source \gls{arb} is the \gls{arb} that represents
    the current state. This \gls{arb} will be compared to the target \gls{arb},
    generating the edit script that transforms this \gls{arb} into the target
    one
  }}
\nge{trg}{name=target,parent=arb,description={
    The target \gls{arb} is the \gls{arb} that represents the
    objective state. This \gls{arb} will be compared to the source \gls{arb},
    generating the edit script that transforms the source \gls{arb} into this
    one
  }}
\nge{scr}{name=script,parent=arb,description={
    The edit script is an \gls{arb} that represents the changes from the
    \gls{src} \gls{arb} to the \gls{target} \gls{arb}.
  }}
\nge{val}{name=value,parent=arb,description={
    For a non-leaf node, the value is an attribute of the node that stores a
    reference to a \gls{prmval}.
    For a leaf node, the value is a \gls{cmpval} represented by the node's type
    and subtree
  }}
\nge{typ}{name=type,parent=arb,description={
    All nodes are colored with a type, that describes the type of the node's
    \gls{val}. This information is necessary for non-leaf nodes to determine how
    to interpret the labels of their children, is used for type-based rule-matching,
    and can be used to type-check the multiple operations done on the \gls{arb}.
  }}
\nge{key}{name=key,parent=arb,description={
    All non-root nodes can have a key, that describes the relation of a node to
    its parent. For example, if the node stores a \gls{val} in a mapping, the
    node's key would be the key of the \gls{val} in the mapping. This
    information is necessary to be able to represent the relation of
    \glspl{cmpval} to their parents and is used for value-based rule-matching.
  }}
\nge{chn}{name=change,parent=arb,description={
    A change is a sequence of operations, where an operation is
    the creation, edition, or deletion of a particular set of nodes. A list of
    changes can be used to represent the difference between two \glspl{arb}.
    Changes are annotated so that they contain a description of what the
    change represents in the external entities that the input data represents
  }}


% Functions
% -----------------------------------------------------------------------------

\nge{fnc}{name=function,description={
    Functions are used to allow rules to specify custom behaviour for each
    operation of the process applied (when evaluating, merging,
    expanding and comparing). Each step of the process is configured using rules
    that map node attributes to constructor these functions. Some base functions are
    provided by \texttt{gdconf}, and custom functions may be defined or loaded
    from external files (as Python code, or external commands)
  }}
\nge{evltr}{name=evaluator,parent=fnc,description={
    When evaluating the \gls{src} data, an evaluator is the constructor of a
    function that evaluates the value of a node from external state. This
    function may determine the \gls{typ}, \gls{key}, \gls{val} of the resulting
    node
  }}

\nge{strat}{name=strategy,plural=strategies,parent=fnc,description={
    When merging the multiple sources for the \gls{src} and \gls{trg}
    data, a strategy is the constructor of a function that merges a sequence of
    nodes into a single one. This function may determine the \gls{typ},
    \gls{key} and \gls{val} of the resulting node. Strategies that apply to
    \gls{cmpval} may require merging their corresponding members, so this
    function may also receive a merge rule to use for this operation
  }}
  
\nge{xpndr}{name=expander,parent=fnc,description={
    When expanding the nodes of an \gls{arb}, an expander is the constructor of
    a function that transforms a single node in another one. This function may
    determine the \gls{typ}, \gls{key} and \gls{val} of the resulting node, and
    may use the \gls{val} of other nodes (a proper resolution order being
    computed before expanding). Expanders that apply to \gls{cmpval} may
    require expanding their members, so this function may also a receive an
    expansion rule to use for this operation
  }}

\nge{cmprr}{name=comparer,parent=fnc,description={
    When comparing the nodes of the \gls{trg} \gls{arb} to the nodes at the same
    \gls{pth} in the \gls{src} \gls{arb}, a comparer is the constructor of a
    function that compares \gls{src} and \gls{trg} \glspl{val}, producing an
    annotated \gls{chn} that describes the operations done on the \gls{src}
    \gls{arb} that make the \gls{src} value become the \gls{trg} value,
    annotated with a description of events that would make this node's
    \gls{evltr} return the \gls{trg} value
  }}

% Rules
% -----------------------------------------------------------------------------

\nge{rl}{name=rule,description={
    Rules are used to specify the behaviour of the parse, read, merge, expansion
    and comparison operations. They map a node \gls{typ} and/or \gls{key} to
    either other rules or to a specific function to use for that node. Because
    rules may contain other rules, they form a hierarchic structure, that is
    represented as an \gls{arb}. Rules of each type can be named, so that some
    rules can be referred to, and a rule with a special name (such as
    ``default'') will be used by default. Some default rules are provided by
    \texttt{gdconf} and may be overriden either by configuration or at run-time
  }}

\nge{evlrl}{name=evaluation rule,parent=rl,description={
    In an \gls{arb}, rule that specifies which \gls{evltr} to use to evaluate
    the current \gls{val} of a node
  }}
  
\nge{mrgrl}{name=merge rule,parent=rl,description={
    In an \gls{arb}, rule that specifies which \gls{strat} when merging multiple
    \glspl{val}
  }}

\nge{xpnrl}{name=expansion rule,parent=rl,description={
    In an \gls{arb}, rule that specifies which \gls{xpndr} to use when
    expanding the nodes
  }}
\nge{cmprl}{name=comparison rule,parent=rl,description={
    In an \gls{arb}, rule that specifies which \gls{cmprr} to use when
    comparing the nodes of a \gls{src} \gls{arb} to the nodes of \gls{trg}
    \gls{arb}
}}


% Documents
% -----------------------------------------------------------------------------

\nge{doc}{name=document,description={
    Object used to represent the input, output and intermediate data. Documents
    can be read and written as files or RPC objects, and are interpreted using
    various formatting languages
  }}
  
\nge{datdoc}{name=data document,parent=doc,description={
    A \gls{parb} that contains the input data trees, representing some
    current or target state. This document is a \gls{parb} and not an
    \gls{arb} because multiple input data trees can be specified. They will be
    merged together in a single data tree, before being expanded and compared to
    another data document. This allows using multiple data sources.
    \texttt{gdconf} uses two kind of data documents : the \gls{src} and
    \gls{trg} data documents, that are compared to generate the output edit
    script document
  }}

\nge{ruldoc}{name=rule document,parent=doc,description={
    An \gls{parb} that contains the \glspl{mrgrl}, the \glspl{xpnrl},
    and the \glspl{cmprl} for \glspl{val} at each \gls{pth} of the data structure. This
    \gls{parb} will be merged into an \gls{arb} using hard-coded rules. As for
    data documents, this allows using multiple data sources
  }}

\nge{mrgdoc}{name=merged document,parent=doc,description={
    An \gls{arb}, produced by applying the merge \glspl{strat} the
    \glspl{datdoc} using the \glspl{mrgrl} in the \glspl{ruldoc}
}}
\nge{xpndoc}{name=expanded document,parent=doc,description={
    An \gls{arb}, produced by applying the expansion \glspl{xpndr} on the
    \gls{mrgdoc} using the \glspl{xpnrl} in the \glspl{ruldoc}
}}
\nge{cmpdoc}{name=comparison document,parent=doc,description={
    A sequence of pairs that contain the annotated \glspl{chn} that transform
    the \gls{src} documents into the \gls{trg} documents, where the
    annotations describe the state changes on the external entities that the
    \gls{src} and \gls{trg} documents represent
  }}