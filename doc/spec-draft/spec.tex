\documentclass{codespec}

\title{The \texttt{gdconf} data format}

\author{Hugo Geoffroy}
\date{\today}

\makeglossaries
\input{./glossary.tex}


\newcommand\YAMLcolonstyle{\color{red}\mdseries}
\newcommand\YAMLkeystyle{\color{black}\bfseries}
\newcommand\YAMLvaluestyle{\color{blue}\mdseries}
\makeatletter
\newcommand\language@yaml{yaml}
\newcommand\ProcessThreeDashes{\llap{\color{cyan}\mdseries-{-}-}}
\expandafter\expandafter\expandafter\lstdefinelanguage
\expandafter{\language@yaml}
{
  keywords={true,false,null,y,n},
  keywordstyle=\color{darkgray}\bfseries,
  basicstyle=\YAMLkeystyle,
  sensitive=false,
  comment=[l]{\#},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{purple}\ttfamily,
  stringstyle=\YAMLvaluestyle\ttfamily,
  moredelim=[l][\color{orange}]{\&},
  moredelim=[l][\color{magenta}]{*},
  moredelim=**[il][\YAMLcolonstyle{:}\YAMLvaluestyle]{:},
  morestring=[b]',
  morestring=[b]",
  literate =    {---}{{\ProcessThreeDashes}}3
                {>}{{\textcolor{red}\textgreater}}1     
                {|}{{\textcolor{red}\textbar}}1 
                {\ -\ }{{\mdseries\ -\ }}3,
}
\lst@AddToHook{EveryLine}{\ifx\lst@language\language@yaml\YAMLkeystyle\fi}
\makeatother

\newcommand{\cbbox}[2]{\fcolorbox{black}{#1}{#2}}
\newcommand{\cfbox}[2]{\fcolorbox{#1}{white}{#2}}

\begin{document}

\maketitle

\emph{This document is a specification of \texttt{gdconf}, a configuration
management tool that operates by comparing source and target arborescences to
determine a graph of commands that change the source configuration into the
target configuration. The source and target arborescences are preprocessed
before being compared, so that multiple values can be provided for each node
(the ``merge'' operation) and so that the values can be transformed (the
``expansion'' operation) before comparison. Every step of the process (including
the merge and expansion) can be configured using ``rules'', that define how
should nodes be merged, expanded and compared, and how to generate the resulting
change graph.}

\vspace{20pt}

\emph{\texttt{gdconf} should provide a proper abstraction for some parts
of the configuration management and deployment process, and define the data
structures and operations that compose this process.}

\vspace{20pt}

\cbbox{orange}{WARNING :} This document is a draft.

\vspace{10pt}

\cbbox{orange}{WARNING :} The current glossary is outdated.

\pagebreak

\tableofcontents

\setlength{\parskip}{\baselineskip}
\setlength{\parindent}{0pt}

\pagebreak
\section{Introduction}

Configuration management can be seen as abstracting configuration as state that
can be modified in a declarative way. Many systems require being configured in
an imperative way (e.g. package managers on many distributions, partition
tables, files on a filesystem, etc). In order to configure these systems in a
declarative way, an abstraction of their state has to be constructed. This
abstraction is used to declare the target (objective) state, and to evaluate
the source (current) state. These two states can then be compared, in order to
generate the changes that transform the system's space from its
source to its target.

For example, in Ansible (and other configuration management tools), writing
custom modules often imply defining such an abstraction for specific subsystems
that cannot be configured in a declarative way. For example, in my configuration
management experience, I've had to do that for ZFS pools (comparing current and
target pool layouts to generate the ZFS commands that transforms a pool with the
current layout to a pool with the target layout), and for Mikrotik devices
(which do not have a proper configuration file, they are configured using a
sequence of imperative shell commands).

\texttt{gdconf} provides an environment that helps defining such an abstraction,
by defining a language that describes subsystem-specific abstractions and
integrates them into an abstraction of the whole system's state. This language
is also used to define interpretation rules for the data abstraction (the
configuration), that allow defining preprocessing operations to apply to the
configurations before comparing them. This preprocessing is used to merge
multiple documents for the source  and target data and expand the data (expand
cross-references in the configuration, so that values can refer to other
values). The preprocessed source and target data is then compared to generate a
change tree, that represents the transformation of the source configuration to
the target configuration, and contains commands that transform the state of a
system from the source state to the target state. 

This abstraction can be used to define a standard configuration format,
compatible with machines of various types, distributions and configurations.
Using \texttt{gdconf} allows to abstract the task of configuration preprocessing
(merging and expansion) and processing (comparison and application), separating
the substance and manner of the concept of ``configuration''.

\pagebreak

\section{Existing work}
\label{sec:conclusion}
\subsection{bash, fabric, custom tooling}

TODO

\subsection{Ansible, Chef, Puppet}

\texttt{gdconf} was written after some time using \texttt{Ansible} for
configuration management, and with relative knowledge of \texttt{Chef} and
\texttt{Puppet}. My usage of \texttt{Ansible} made me think that I could try to
create a tool that provides a more complete way to specify the target
configuration, and to define the abstractions of the system state.

In \texttt{Ansible} the the target configuration is interpreted in a hard-coded
way, and even if this hard-coded way uses Jinja2 for templating, it's hard to
define custom interpretation rules for the configuration. For example, there is
no way to specify how to merge multiple input configurations into a single one,
variable scopes can not be specified, etc. In \texttt{gdconf}, everything is a
``rule'' : rules are used to define how should the current configuration be
evaluated, how the multiple configurations are merged in a single one, how the
merged transformations are expanded (to use arbitrary templating languages, for
example), and how the source and target configurations are compared to the target,
configuration, generating the change tree that contains the commands that
transform a system's state from the source state to the target state.

\pagebreak
\subsection{mgmt}

\texttt{gdconf} is similar to \href{https://github.com/purpleidea/mgmt}{mgmt}, a
declarative, distributed and event-based. I discovered \texttt{mgmt} a bit after
starting to think of the ideas used to write \texttt{gdconf}, and it was a
strong inspiration to my work. The ideas described in the various blog posts
written by its author (James of \href{https://purpleidea.com}{purpleidea.com}),
such as
\href{https://purpleidea.com/blog/2016/01/18/next-generation-configuration-mgmt/}{this
  one}, are in line with what I had in mind for \texttt{gdconf}.

\texttt{mgmt} and \texttt{gdconf} have some big architectural differences :
\begin{itemize}
\item \texttt{gdconf} is built as a pure comparison of preprocessed graphs using
  provided interpretation rules, while \texttt{mgmt} uses predefined, hardcoded
  rules
\item \texttt{gdconf} should be able to provide a more flexible data description
  language (although, like in \texttt{mgmt}, the language should not be
  Turing-complete).
\item \texttt{gdconf} works on a arborescence that represents the (as complete
  as possible)
  configuration of the managed system, whether it is a single service, node,
  cluster, or site. This arborescence should contain 
\item \texttt{gdconf} will not be initially event-based, although ``watching''
  rules could be added at a later stage
\item \texttt{gdconf} will not be initially distributed, although the graph
  could be shared using some distributed consensus protocol
\end{itemize}

If watching rules and a proper distribution protocol are implemented,
\texttt{gdconf} will provide (like \texttt{mgmt}) a complete environment for the
definition of an integrated monitoring and configuration management system.

\pagebreak
\section{Definitions}

This document makes use of various terms with specific meanings that are related
to the concepts used in \texttt{gdconf}. These terms are related with the name
of the objects and variables that will be used in its implementation.

The terms and concepts used in this document are defined in three parts :
\begin{itemize}
\item In the descriptions of the data structures and their relations.

  (in this section)
  
\item In the specifications of the operations and their rules.
  
  (in \boxref{sec:concepts})
  
\item In a glossary, that describes all specific terms used in this document.

  (at the end of the document)

  (words can be links to the glossary, \emph{e.g.}: \gls{imm})
\end{itemize}

The following subsections provide a short description of the various data
structures used in \texttt{gdconf}.

\begin{note}
  The following descriptions contain references to
concepts and operations that will be described later in this document, so they
do not provide a complete description of the data structures, and should be
seen as helpers for reading this document without having to do too much guesswork
on the vocabulary.
\end{note}
\pagebreak

\subsection{\Gls{val} kinds}

\begin{description}
\item[A \gls{prmval}] is a value of a primitive data type.

  (such as \texttt{bool},\texttt{str},\texttt{int}, \texttt{float}, etc)
  
\item[A \gls{cmpval}] is a value that is formed of other values.
  
  (either \textbf{atomic} or \textbf{composite})
  
\item[An \gls{atmval}] is a value that is considered as a whole.

  (without deconstructing its members)

  This value can be a \gls{prmval}, but can also be a \textbf{composite value}
  that we choose to consider as an \textbf{atomic value}.
\end{description}
\subsection{\Glspl{arb}}
\begin{description}
  
\item[An \gls{arb}] is a labeled tree with edges pointing from its root.

  (formed of nodes that can be labelled by a \gls{typ}, \gls{key}, and \gls{val})
  
\item[Non-root nodes of an \gls{arb}] \uline{can} be labeled by a ``\gls{key}''

  (that maps them in their parent, used to identify members of
  \textbf{\glspl{cmpval}}, such as \texttt{dict}, \texttt{tuple}, \texttt{list},
  etc).
  
\item[Non-leaf nodes of an \gls{arb}] \uline{must} be labeled by a ``\gls{typ}''

  (used to deconstruct \textbf{\glspl{cmpval}}, their members being stored in
  the node's subgraph).
  
\item[Leaf nodes of an \gls{arb}] \uline{must} be labeled by a ``\gls{val}''

  (the \textbf{\gls{atmval}} that they represent.)

\item[\Glspl{arb}] can be used to represent \textbf{\glspl{cmpval}}.

\end{description}
\pagebreak
\subsection{Main data types}
\begin{description}
  
\item[The \gls{rl} data] is an ordered sequence of \textbf{\glspl{arb}}.

  It is provided as input to the program , combined to default definitions,
  preprocessed and used to define the behaviour of the preprocessing
  (\textbf{merge}/\textbf{expansion}) and processing (\textbf{comparison})
  operations (for the \textbf{\gls{src}} and \textbf{\gls{trg} data}).
  
\item[The \gls{src} data] is an ordered sequence of \textbf{\glspl{arb}}.

  It is evaluated from remote state (or provided as input), that are preprocessed to
  form the \textbf{\gls{src} state}, that will be compared with the
  \textbf{\gls{src} state} to generate the \textbf{\gls{chn} script}.
  
\item[The \gls{trg} data] is an ordered sequence of \textbf{\glspl{arb}}.

  It is provided as input to the program (or read from a remote source), and preprocessed to form the
  \textbf{\gls{trg} state}, that will be compared with the \textbf{\gls{src}
  state} to generate the \textbf{\gls{chn} script}.

\item[The \gls{chn} data] is a tree of \textbf{\glspl{chn}}.

  A \textbf{\gls{chn}} describes a transformation of an \textbf{\gls{arb}}
  annotated with a description of the \textbf{\gls{chn}} as an operation on the
  remote system.

  This tree is a directed and ordered tree, where \textbf{\glspl{chn}} depend
  on their parents.

\end{description}
\pagebreak
\subsection{Intermediate data types}
\begin{description}
  
\item[The \gls{rl} data] is merged to form a single \textbf{\gls{arb}}.

  This \textbf{\gls{arb}} describes the nodes \textbf{\gls{src}} and
\textbf{\gls{trg} data} and specifies the \textbf{\glspl{strat}} to use for
merging, the \textbf{\glspl{xpndr}} to use for expanding, and the
\textbf{\glspl{chn}} to use for comparing.
\item[The \gls{trg} and \gls{src} data] are merged in a single \textbf{\gls{arb}}.

  (to form the \textbf{the merged \gls{trg} and \gls{src} data})
  
  (using the \textbf{\glspl{mrgrl}} in the \textbf{merged \gls{rl} data})
  
  
\item[The merged \gls{trg} and \gls{src} data] are expanded.
  
  (to form the \textbf{the expanded \gls{trg} and \gls{src} data})

  (using the \textbf{\glspl{xpnrl}} in the \textbf{merged \gls{rl} data})
  
\item[The expanded \gls{trg} and \gls{src} data] are compared.

  (to generate the \textbf{the annotated \gls{chn} tree})
  
  (using the \textbf{\glspl{cmprl}} in the \textbf{merged \gls{rl} data})

\item[The annotations of the \gls{chn} tree]  describe remote operations.

  (that change the remote state so that it evaluates as the \textbf{\gls{trg}
    state})

  (that will be executed, or printed-out if in ``dry-run'')
  
\end{description}

\pagebreak

\section{Concepts}
\label{sec:concepts}

\subsection{Configuration as an arborescence}
\label{sec:cod}

The following listings describe an example target configuration as multiple YAML
documents, showcasing some of the features that \texttt{gdconf} should provide.

\begin{lstlisting}[label=lst:cfg-dmn,caption=Domain configuration (YAML)]

domain:
  name: example.org
  nameservers:
   - 10.0.0.1
   - 10.0.0.2
  packages:
   - domain-pkg1
  files:
    /etc/motd: "Welcome to example.org !"
\end{lstlisting}

\newcommand{\nodelabel}[1]{Label(\texttt{#1})}
\newcommand{\nodetype}[1]{Type(\texttt{#1})}
\newcommand{\nodevalue}[1]{Value(\texttt{#1})}

\begin{figure}[h]
  \caption{Domain configuration (as an \gls{arb})}
  \label{fig:cfg-dmn}
  \begin{tikzpicture}
    \tikzstyle{every node}=[rectangle,draw=black]
    \small
    \node (root) at (0, 1) {\nodetype{root}};
    \node (dmn) at (2, 0) {\makecell{\nodelabel{domain}\\\nodetype{dict}}};
    \node (ns) at (4, -1.5) {\makecell{\nodelabel{nameservers}\\\nodetype{list}}};
    \node (pkg) at (4, -4.5) {\makecell{\nodelabel{packages}\\\nodetype{list}}};
    \node (fls) at (4, -6) {\makecell{\nodelabel{files}\\\nodetype{dict}}};

    \node (nm) at (9, 0) {\makecell{\nodelabel{name}\\\nodevalue{example.org}}};
    \node (ns1) at (9, -1.5) {\makecell{\nodelabel{0}\\\nodevalue{10.0.0.1}}};
    \node (ns2) at (9, -3) {\makecell{\nodelabel{1}\\\nodevalue{10.0.0.2}}};
    \node (pkg1) at (9, -4.5) {\makecell{\nodelabel{0}\\\nodevalue{domain-pkg1}}};
    \node (motd) at (9, -6) {\makecell{\nodelabel{/etc/motd}\\\nodevalue{Welcome to example.org !}}};

    \coordinate (root-dmn) at (0, 0);
    \coordinate (dmn-ns) at (2, -1.5);
    \coordinate (dmn-pkg) at (2, -4.5);
    \coordinate (dmn-fls) at (2, -6);
    \coordinate (ns-ns2) at (4, -3);

    \draw[-] (root) -- (root-dmn);
    \draw[->] (root-dmn) -- (dmn);
    
    \draw[-] (dmn) -- (dmn-ns);
    \draw[-] (dmn) -- (dmn-pkg);
    \draw[-] (dmn) -- (dmn-fls);

    \draw[->] (dmn) -- (nm);
    \draw[->] (dmn-ns) -- (ns);
    \draw[->] (dmn-pkg) -- (pkg);
    \draw[->] (dmn-fls) -- (fls);
    \draw[->] (ns) -- (ns1);
    \draw[-]  (ns) -- (ns-ns2);
    \draw[->] (ns-ns2) -- (ns2);
    \draw[->] (pkg) -- (pkg1);
    \draw[->] (fls) -- (motd);
  \end{tikzpicture}
\end{figure}

\pagebreak

The configuration described in \cref{lst:cfg-dmn} is also represented as an
\gls{arb} in \cref{fig:cfg-dmn}. This configuration represents common
settings used for machines in a domain. We can assume that theses nodes are only
used to store data used in other nodes, so no evaluation or comparison rules
will be defined for them.

\begin{lstlisting}[label=lst:cfg-net,caption=Networks configuration]
networks:
  subnets:: '192.168.0.0/16'|ip4_subnets(24)
  interfaces:
    default-net:
      range:: subnet[1:-2]
      gateway:: subnet[-2]
      subnet:: networks.subnets|pool
    default-static:
      !expand: default-net
      type:: static
      address:: range|pool
    default-dhcp:
      !expand: default-net
      type:: dhcp
\end{lstlisting}

The configuration described in \cref{lst:cfg-net} represents common
``templates'' used for network interface definitions. We can make multiple
observations on this listing :
\begin{itemize}
  \item \texttt{networks} is a mapping with 3 keys : \texttt{default-net},
    \texttt{default-static} and \texttt{default-dhcp}
  \item \texttt{default-static} and \texttt{default-dhcp} contain a special
    \texttt{!merge} key used to include the contents of \texttt{default-net}
  \item Some values and keys are separated using a double semicolon, used to
    indicate that the value is a Jinja2 expression that should be evaluated to a
    native type.
\end{itemize}
    
\pagebreak
\begin{lstlisting}[label=lst:cfg-tpl,caption=Machine template]
templates.default:
  system:
    hostname: {{name}}.{{domain.name}}
    nameservers:: domain.nameservers
    packages:
      - !expand domain.packages
      - test-pkg1
      -- not(test-pkg3)
      - name: test-pkg4
        state:: absent
      - test-pkg5/stable
      - name: test-pkg5
        version: stable
    files:
      !expand: domain.files
      /etc/test-pkg1/test-pkg1.conf:
        !require: system.packages.test-pkg1
        replace:
          'hostname = localhost': 'hostname = {{name}}'
  network.interfaces:
    eth0:
      !expand: networks.default-dhcp
    eth1:
      !expand: default-static
      subnet: 10.0.0.0/24
\end{lstlisting}
\pagebreak

The configuration described in \cref{lst:cfg-tpl} represents a template used for
machines that defines settings, packages, files and network interfaces. We can
make some additional observations on this listing :
\begin{itemize}
\item Mapping keys can be paths to member of inner mappings (such as
  \texttt{templates.default}, that represents the \texttt{default} key in the
  mapping at the \texttt{templates} key).
  \item Some values contain expressions delimited by \texttt{\{\{} and
    \texttt{\}\}}. They represent Jinja2 templates that should be evaluated to a
    string.
  \item Some list values are prefixed by a double dash, used to indicate that
    the value is a Jinja2 expression that should be evaluated to a native type.
  \item We used \texttt{!merge} to include the contents of \texttt{default-dhcp}
    and \texttt{default-static} in \texttt{eth0} and \texttt{eth1}.
  \item Some dictionaries have a special key, \texttt{!require}, used to
    indicate dependencies between nodes of the configuration \gls{arb}. These
    dependencies are used for to determine the order in which the evaluation and
    comparison rules are applied.
  \end{itemize}

\begin{lstlisting}[label=lst:cfg-mch,caption=Templated machines]
machines:
  node1, node2, node3: !expand templates.default
  node3.system.packages: ['openssh-server']
\end{lstlisting}

The configuration described in \cref{lst:cfg-mch} represents a list of three
machines built from the template in the previous listing, adding a package in
the third machine. We can make some additional observations on this listing :
\begin{itemize}
\item The value of multiple keys can be specified at the same time, meaning that
  each key will be set to the specified value.
  \item The value of an already defined key may be redefined in later keys (such
  as \texttt{node3.system.packages} redefining \texttt{node3}, and both
  definitions will be merged together using the merge rules.
\end{itemize}

When fully preprocessed, these configurations would be equivalent to the
following configuration :

\begin{lstlisting}[label=lst:cfg-full,caption=Preprocessed configuration]
machines:
  node1, node2, node3: !expand(templates.default)
  node3.system.packages: ['openssh-server']
\end{lstlisting}

Applying the given configuration with \texttt{gdconf} would require a set of
evaluation, merge, expansion, and comparison rules that provide the language
features used in the configuration :
\begin{description}[style=nextline,parsep=0pt,itemsep=2pt]
\item[Jinja2 string templates]
  \texttt{\emph{key}: \{\{ \emph{expression} \}\}} \hfill (formatted string)
\item[Jinja2 literal expressions]
  \texttt{\emph{key}:: \emph{expression}}  \hfill (typed expression)
\item[Symbols]
  \texttt{absent}, \texttt{present}, \texttt{dhcp}, \texttt{static} \hfill
  (static symbols)
\item[Expansion tag]
  \texttt{!expand \emph{key}} \hfill (to expand in a list)
  
  \texttt{!expand: \emph{key}} \hfill (to expand in a dictionary)
\item[Pool filter]
  \texttt{\emph{key}:: \emph{value}|pool} \hfill (becomes unique when expanded)
\item[IPv4 subnets filter]
  \texttt{\emph{key}:: '\emph{subnet}'|ip4\_subnets(\emph{mask})} \hfill (enumerates subnets)
\end{description}


Note that the base syntax is defined by
\texttt{gdconf} (in the definition of \glspl{arb}), and can not be customized by
any rule :
\begin{description}[style=nextline,parsep=0pt,itemsep=2pt]
\item[Arborescence structure]
  composite values (\texttt{list}, \texttt{dict}, \texttt{set})
  
  primitive values (\texttt{str}, \texttt{int}, \texttt{float}, \texttt{bool})
\item[Tag syntax]
  \texttt{!tag}
\item[Multiple assignments]
  \texttt{a, b, c: foo} 
\end{description}

\pagebreak 

\subsection{Rules}
\begin{warn}TODO\end{warn}
\subsection{Evaluation}
\begin{warn}TODO\end{warn}
\subsection{Merging}
\begin{warn}TODO\end{warn}
\subsection{Expansion}
\begin{warn}TODO\end{warn}
\subsection{Comparison}
\begin{warn}TODO\end{warn}
\pagebreak
\section{Specification}
\label{sec:spec}

\subsection{Program specifications}

\begin{warn}TODO

\begin{itemize}
\item Define the general specifications of a program that would implement the
process described in the introduction.
\end{itemize}
\end{warn}

\subsection{Typing specifications}

\begin{warn}TODO
\begin{itemize}
\item Define typing specifications of the objects used in the specified program,
adapted to the multiple stages of the process.
\end{itemize}
\end{warn}

\pagebreak
\section{Implementation}
\label{sec:pythonimp}

\begin{warn}TODO
\begin{itemize}
\item Describe the general architecture of a Python implementation that would
  implement the program and typing specifications.
\end{itemize}
\end{warn}

\pagebreak
\section{Conclusion}
\label{sec:conclusion}

\begin{warn}TODO
\begin{itemize}
\item Write a conclusion that synthetizes the descriptions and specifications
  provided by this document, and mentions further way to improve this document
  and the described program.
\end{itemize}
\end{warn}

\pagebreak

\begin{warn}The current version of the glossary is out of date, and the
  definitions may not match their latest usage. This will be fixed soon.
\end{warn}
\printglossaries

\pagebreak
\section{Links}
\label{sec:links}

\begin{warn}TODO\end{warn}

\end{document}