# gdconf

[![Stability: experimental](https://img.shields.io/badge/stability-experimental-orange.svg)]()
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Python: 3.5+](https://img.shields.io/badge/python-3.5%2B-blue.svg)](https://www.python.org/)

**This project is a Work In Progress, and is not ready for any kind of use yet.**

gdconf a is configuration management tool, used to abstract system state as
configuration, so that we can define a configuration as a representation of a
system's state, and compare source (current) and target (objective)
configurations to generate a list of changes that transform the source
configuration in the target configuration, annotated with descriptions of these
changes as operations on the abstracted system state (e.g., shell commands).
This provides an purely declarative interface to the system's imperative
interface.

The target configuration is provided by the user, and the source configuration
is evaluated from the current source state of the remote system. These
configurations are preprocessed, then compared to build a change tree that is
traversed, executing the operations in its nodes on the remote system.

The preprocessing allows the source and target configurations to be defined in
multiple documents (that are merged in a single configuration) and expanded by
various predefined transformations. The behaviour of the evaluation, merge,
expansion, and comparison steps is defined by rules, that are provided by the
user.

This allows to generate a sequence of operations that would apply a target
configuration to a system with a given source configuration.
review the sequence of changes before applying them.

## Documents

- [The `gdconf` specification](./doc/spec-draft/spec.pdf) : First specification 
  of the data format and operations in `gdconf`
