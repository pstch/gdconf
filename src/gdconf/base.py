# pylint: disable=missing-docstring,too-few-public-methods
"""TODO

"""
# Imports & typing
# =============================================================================
from typing import Iterable, List
from abc import ABC, abstractmethod

from .typedefs import File, Data, Item
# from .graph import File, Data


# Base classes
# =============================================================================

class Interface(ABC):

    @abstractmethod
    def read(self, file: File) -> Data:
        pass

    @abstractmethod
    def write(self, file: File, data: Data) -> None:
        pass


class BaseTag:

    def __init__(self, **kwargs: Data) -> None:
        pass

    def _to_items(self) -> List[Item]:
        return list(self.__dict__.items())

    @classmethod
    def _from_items(cls, items: Iterable[Item]) -> 'BaseTag':
        return cls(**dict(items))  # no type
