"""This module provides the core traversal function, used to read and write the
data structures used by gdconf (arbitrary structures of nested dicts, lists,
tags and primitive data types).

By default, :func:`traverse` just copies an arbitrarily nested data structure
using the given unpacking/repacking strategy. It can be extended by providing
that will be called at specific points of the traversal.

>>> from .interface_yaml import yaml_load
>>> with open('./rule_draft.yml') as file:
...     data = yaml_load(file)
...     assert traverse(data) == data

"""
# Imports
# =============================================================================
from typing import cast, Union, Optional, List, Set
from types import MappingProxyType
from dataclasses import dataclass, field

from .base import BaseTag
from .utils import list_values
from .typedefs import Label, Type, Data, Item, Strategy

# Constants
# =============================================================================

# pylint: disable=protected-access
STRATEGY = cast(Strategy, MappingProxyType({
    BaseTag: (BaseTag._to_items, BaseTag._from_items),
    dict: (dict.items, dict),
    list: (enumerate, list_values)
}))
# pylint: enable=protected-access


# Traversal function (and associated dataclass)
# =============================================================================

def traverse(data: Data, strategy: Strategy = STRATEGY) -> Data:
    """Rebuild a data structure by traversing it, using the given strategy to
    unpack and repack its nodes, and calling the given hooks at appropriate
    times. NOTE: algorithm used does not support loops in the input graph.

    The strategy is a mapping of types names to a 2-tuple containing unpacking
    and repacking functions. The default strategy provides support for dicts,
    lists and tags, traversing all of their values.

    :argument Data data: The input data to be unpacked by the given strategy
    :argument Strategy strategy: Dict of types to unpacking/repacking functions
    :returns Data: The output data to be repacked by the given strategy
    :raises ValueError: When the input data contains loops.

    >>> TEST_DATA = [
    ...   None,
    ...   [1],
    ...   [1+2j, 1, 1.0],
    ...   ["a","b","c"],
    ...   {1: ["a"], 2: {}, 3: None},
    ...   {1: {2: {3: {4: {5: [[[[[True]]]]]}}}}}
    ... ]
    >>> for data in TEST_DATA:
    ...   assert traverse(data) == data

    """
    @dataclass
    class Frame:
        """Inner class for the traversal algorithm's queue."""
        # __slots__ = 'data', 'target', 'label', 'type', 'items'
        data: Union[Data]
        target: List[Item]
        label: Label = ''
        type: Type = field(init=False)
        items: Optional[List[Item]] = None

        def __post_init__(self) -> None:
            self.type = type(self.data)

    target: List[Item] = list()
    seen: Set[int] = set()
    stack = [Frame(data, target)]
    while stack:
        frame = stack.pop()
        if frame.items is None:
            # first stage : find unpacker or yield to target
            try:
                unpack = strategy[frame.type][0]
            except KeyError:
                # no unpacker, yield value to target
                frame.target.append((frame.label, frame.data))
            else:
                # found unpacker, recurse in unpacked items
                data_id = id(frame.data)
                assert data_id not in seen, "loop in input data"
                seen.add(data_id)
                frame.items = list()
                stack.append(frame)
                for label, _data in unpack(frame.data):
                    stack.append(Frame(_data, frame.items, label))
                frame.data = None
        else:
            # second stage : find repacker and yield to target
            repack = strategy[frame.type][1]
            value = repack(reversed(frame.items))
            frame.target.append((frame.label, value))
    assert len(target) == 1
    return target[0][1]
