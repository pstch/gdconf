"""Utility module.

"""
from typing import List
from .typedefs import Item, Data


def list_values(items: List[Item]) -> List[Data]:
    """List the values of the given items."""
    return list(value for _, value in items)
