# pylint: disable=missing-docstring,unused-argument
"""TODO"""
from sys import stderr

import click
import logbook

logger = logbook.Logger(__name__)  # pylint: disable=invalid-name


@click.group()
@click.option('--debug/--no-debug', default=False)
def cli(debug):
    if debug:
        logger.debug("debug mode enabled")


@cli.command(help="Check that rules apply properly on the configuration")
@click.argument('config', nargs=-1)
@click.argument('rules', nargs=1)
def check(config, rules):
    pass


@cli.command(help="Merge multiple files in a single configuration")
@click.argument('config', nargs=-1)
@click.argument('rules', nargs=1)
def merge(config, rules):
    pass


@cli.command(help="Evaluate a host's current configuration")
@click.argument('rules', nargs=1)
@click.argument('host', nargs=1)
def eval(rules, host):  # pylint: disable=redefined-builtin
    pass


@cli.command(help="Evaluate and compare a host's configuration")
@click.argument('config', nargs=-1)
@click.argument('rules', nargs=1)
@click.argument('host', nargs=1)
def compare(config, rules, host):
    pass


@cli.command(help="Apply a configuration on a host ")
@click.argument('config', nargs=-1, type=click.Path(exists=True))
@click.argument('rules', nargs=1)
@click.argument('host', nargs=1)
def apply(config, rules, host):
    pass


if __name__ == '__main__':
    import sys
    sys.argv = ['w00t', 'apply', 'fooshit', 'foofuck']
    logbook.StreamHandler(stderr).push_application()
    cli()  # pylint: disable=no-value-for-parameter
