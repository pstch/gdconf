"""TODO

"""
# Typing definitions
# =============================================================================

from typing import TypeVar, Any, Union, Callable, Mapping, Iterable
from typing import Tuple, Dict, List

__all__ = [
    'File', 'Text', 'Type',
    'Value', 'Label',
    'TextTag', 'DataTag',
    'Data', 'Item',
    'Unpacker', 'Repacker',
    'Strategy'
]

# pylint: disable=invalid-name
File = TypeVar('File')
Text = str
Type = type

Value = Union[bytes, str, bool, int, float]
Label = str

TextTag = Any
DataTag = Any

Data = Union[Dict[Label, 'Data'], List['Data'], TextTag, DataTag, Value]
Item = Tuple[Label, Data]

Unpacker = Callable[[Data], Iterable[Item]]
Repacker = Callable[[Iterable[Item]], Data]

Strategy = Mapping[Type, Tuple[Unpacker, Repacker]]

Format = Union[Tuple[Label, 'Format'], Tuple['Format'], Text]
Grammar = Union[Dict[Label, 'Grammar'], List['Grammar'], Text]
# pylint: enable=invalid-name
