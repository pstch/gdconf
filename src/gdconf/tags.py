# pylint: disable=missing-docstring,too-few-public-methods
"""TODO

"""
# Imports
# =============================================================================
from typing import Optional
from dataclasses import dataclass

from .typedefs import Text, Format, Grammar
from .base import BaseTag

__all__ = [
    'ForallTag',
    'MergeTag',
    'ExtendTag',
    'ExpandTag',
    'TargetTag',
    'ExecTag',
    'FileTag',
]


# Tag classes
# =============================================================================

@dataclass
class ForallTag(BaseTag):
    value: str


@dataclass
class MergeTag(BaseTag):
    value: str


@dataclass
class ExtendTag(BaseTag):
    value: str


@dataclass
class ExpandTag(BaseTag):
    value: str


@dataclass
class TargetTag(BaseTag):
    value: str


@dataclass
class FileTag(BaseTag):
    path: str
    format: Optional[Format] = None
    grammar: Optional[Grammar] = None


@dataclass
class ExecTag(BaseTag):
    format: Format
    add: Optional[Text] = None
    remove: Optional[Text] = None
    present: Optional[Text] = None
    list: Optional[Text] = None
