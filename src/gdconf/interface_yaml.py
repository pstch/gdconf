# pylint: disable=missing-docstring

"""TODO

>>> with open("./rule_draft.yml") as file:
...    text = file.read()
...    data = yaml_load(text)
...    assert yaml_dump(data) == text

"""
from typing import Any
import warnings

from ruamel import yaml

from .typedefs import Text, Data
from . import tags



def yaml_load(text: Text) -> Data:
    # NOTE: ruamel does not support custom tags with safe_load
        warnings.simplefilter('ignore', yaml.error.UnsafeLoaderWarning)
        return yaml.load(text)


def yaml_dump(data: Data) -> Any:
    return yaml.dump(data, default_flow_style=False)


class TextTag(yaml.YAMLObject):
    yaml_tag: Text = ''
    value_cls = None
    yaml_loader = yaml.SafeLoader

    def __init__(self, value: Text) -> None:
        self.value = value

    # pylint: disable=arguments-differ,unused-argument
    @classmethod
    def from_yaml(cls, loader: yaml.Loader, node: yaml.Node) -> 'TextTag':
        return cls(node.value)

    @classmethod
    def to_yaml(cls, dumper: yaml.Dumper, data: 'TextTag') -> Any:
        return dumper.represent_scalar(cls.yaml_tag, data.value, style="")
    # pylint: enable=arguments-differ,unused-argument

    def __hash__(self) -> int:
        return hash(self.value)


class DataTag(yaml.YAMLObject):
    yaml_tag: Text = ''
    yaml_loader = yaml.SafeLoader

    def __init__(self) -> None:
        pass


class ForallTag(TextTag, tags.ForallTag):
    yaml_tag = '!forall'


class MergeTag(TextTag, tags.MergeTag):
    yaml_tag = '!merge'


class ExtendTag(TextTag, tags.ExtendTag):
    yaml_tag = '!extend'


class ExpandTag(TextTag, tags.ExpandTag):
    yaml_tag = '!expand'


class TargetTag(TextTag, tags.TargetTag):
    yaml_tag = '!target'


class FileTag(DataTag, tags.FileTag):
    yaml_tag = '!file'


class ExecTag(DataTag, tags.ExecTag):
    yaml_tag = '!exec'
