#!/usr/bin/env python
# pylint: disable=missing-docstring

from distutils.core import setup

setup(name='gdconf',
      version='0.0.0',
      description="rule-based configuration management tool",
      long_description="gdconf is a rule-based monitoring and configuration"
      " management tool, where rules annotate a graph of the configuration and"
      " describe how to evaluate and compare its nodes. \n\n"
      "This package provides a Python implementation of the gdconf library, "
      " and a command-line interface that can be used to evaluate, compare"
      " and apply configuration data on a remote node.",
      author="Hugo Geoffroy (pistache)",
      author_email='gdconf@pstch.net',
      url='https://pstch.net/gdconf/',
      download_url='https://pstch.net/gdconf/latest.tgz',
      license="GNU General Public License v3 or later (GPLv3+)",
      classifiers=["License :: OSI Approved :: "
                   "GNU General Public License v3 or later (GPLv3+)",
                   "Programming Language :: Python :: 3.7",
                   "Development Status :: 1 - Planning",
                   "Intended Audience :: System Administrators",
                   "Topic :: System :: Systems Administration"
                   "Topic :: System :: Distributed Computing",
                   "Topic :: System :: Monitoring",
                   "Environment :: Console"],
      keywords=['config', 'rule', 'configuration', 'deployment', 'management'],
      packages=['gdconf'],


)
