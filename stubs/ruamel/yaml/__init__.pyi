from typing import *

from . import error

File = TypeVar('File')
Value = TypeVar('Value')

Data = Any

class Node:
    value: Data

class Loader: ...
class Dumper:
    def represent_scalar(self, tag: str, value: Value, style: str=None) -> Data: ...

class SafeLoader(Loader): ...

class YAMLObject:
    loader: Loader

def load(text: Text) -> Data: ...
def dump(data: Data, default_flow_style: bool=True) -> Text: ...

