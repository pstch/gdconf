from typing import Type


class Warning: ...


def simplefilter(action: str, category: Type[Warning]=Warning,
                 lineno: int=0, append: bool=False) -> None: ...
